import { createStore, Store } from 'redux';
import rootReducer from './reducers';

export default function configureStore(/*preloadedState*/) {
  //const middlewares = [loggerMiddleware, thunkMiddleware]
  //const middlewareEnhancer = applyMiddleware(...middlewares)
  //const enhancers = [middlewareEnhancer, monitorReducersEnhancer]
  //const composedEnhancers = compose(...enhancers)
  //const store = createStore(rootReducer, preloadedState, composedEnhancers)
  const store = createStore(
    rootReducer, 
    /*preloadedState,*/
    (window as any).__REDUX_DEVTOOLS_EXTENSION__ && (window as any).__REDUX_DEVTOOLS_EXTENSION__()
  );
  
  return store
}
