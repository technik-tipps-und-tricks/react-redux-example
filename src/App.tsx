import React from 'react';
import logo from './logo.svg';
import './App.css';
import { useDispatch, useSelector } from 'react-redux';
import { AppState } from './reducers/appReducer';
import { UserState } from './reducers/userReducer';
import { setSelectedLanguage } from './actions/appActions';
import { setFirstName, setLastName } from './actions/userActions';

function App() {
  const dispatch = useDispatch();
  const selectedLanguage = useSelector((state: { app: AppState}) => state.app.selectedLanguage);
  const lastName = useSelector((state: { user: UserState}) => state.user.lastName);
  const firstName = useSelector((state: { user: UserState}) => state.user.firstName);

  var inputSelectedLanguage: any;
  const onSelectedLanguageChange = (newSelectedLanguage: string): void => {
      dispatch(setSelectedLanguage(newSelectedLanguage));
  };

  var inputFirstName: any;
  const onChangeFirstName = (newFirstName: string): void => {
    dispatch(setFirstName(newFirstName));
  };

  var inputLastName: any;
  const onChangeLastName = (newLastName: string): void => {
    dispatch(setLastName(newLastName));
  };

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.tsx</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
      <body className="App-body">
        <h1>Stored app values</h1>
        <p><span className="textBold">Language: </span>{selectedLanguage}</p>
        <form onSubmit={e => {e.preventDefault()
          if (!inputSelectedLanguage.value.trim()) {
            return
          }
          onSelectedLanguageChange(inputSelectedLanguage.value);
          inputSelectedLanguage.value = 'default';
          }}>
          <select ref={node => (inputSelectedLanguage = node)} id="changeSelectedLanguage">
            <option value="default" disabled selected hidden>select language...</option>
            <option value="de">Deutsch</option>
            <option value="en">English</option>
            <option value="pl">Polski</option>
          </select>
          <button className="changeButton" type="submit">change</button>
        </form>
        <h1>Stored user values</h1>
        <p><span className="textBold">First Name: </span>{firstName}</p>
        <form onSubmit={e => { e.preventDefault()
          if (!inputFirstName.value.trim()) {
            return
          }
          onChangeFirstName(inputFirstName.value);
          inputFirstName.value = '';
        }}>
          <input ref={node => (inputFirstName = node)} id="changeFirstName" 
            type="text" placeholder="first name..." />
          <button className="changeButton" type="submit" value="change">change</button>
        </form>
        <p><span className="textBold">Family Name: </span>{lastName}</p>
        <form onSubmit={e => {
          e.preventDefault()
          if (!inputLastName.value.trim()) {
            return
          }
          onChangeLastName(inputLastName.value);
          inputLastName.value = '';
        }}>
          <input ref={node => (inputLastName = node)} id="changeFamilyName" 
            type="text" placeholder="last name..." />
          <button className="changeButton" type="submit" value="change">change</button>
        </form>
      </body>
      <footer className="App-footer">
        <p className="footerText">
          <a className="footerLink" href="http://www.technik-tipps-und-tricks.de" 
            target="_blank" rel="noopener noreferrer">Technik Tipps und Tricks</a>
        </p>
      </footer>
    </div>
  );
}



export default App;
