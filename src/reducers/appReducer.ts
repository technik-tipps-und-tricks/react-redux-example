import { SET_SELECTED_LANGUAGE, AppActionTypes } from '../actions/appActions';

export interface AppState {
  selectedLanguage: string;
}

const initialState: AppState = {
  selectedLanguage: 'de',
};

const appReducer = (
  state = initialState,
  action: AppActionTypes,
): AppState => {
  switch (action.type) {
    case SET_SELECTED_LANGUAGE:
      return {
        ...state,
        selectedLanguage: action.payload.selectedLanguage,
      };
    default:
      return state;
  }
};

export default appReducer;