export const SET_SELECTED_LANGUAGE = 'APP/SET_SELECTED_LANGUAGES';

export type AppActionTypes = SetSelectedLanguage;

interface SetSelectedLanguage {
  type: typeof SET_SELECTED_LANGUAGE;
  payload: SetSelectedLanguagePayload;
}

interface SetSelectedLanguagePayload {
  selectedLanguage: string;
}


export function setSelectedLanguage(selectedLanguage: string): SetSelectedLanguage {
  return {
    type: SET_SELECTED_LANGUAGE,
    payload: {
      selectedLanguage,
    },
  };
}