# React Redux Example

An example how to use Redux state container within the React web app framework.

---------------------  
1.  Clone the GitLab-Repository:  
<pre style="color: #646464">
  git clone https://gitlab.com/technik-tipps-und-tricks/react-redux-example.git
</pre>  

2.  Go to project folder and install dependencies:  
<pre style="color: #646464">
  cd react-redux-example
  npm install
</pre>  

3.  Run application:
<pre style="color: #646464">
  npm start
</pre>
